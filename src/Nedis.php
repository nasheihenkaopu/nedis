<?php
namespace Nshkp;

class Nedis
{
    private static $redis;

    private function __construct(){}

    public static function getInstance()
    {
        if(!static::$redis){
            $host = getenv('REDIS_HOST');
            $port = getenv('REDIS_PORT');
            $auth = getenv('REDIS_AUTH');

            $redis = new \Redis();

            if ($redis->connect($host, $port) == false) {
                die($redis->getLastError());
            }

            if ($redis->auth($auth) == false) {
                die($redis->getLastError());
            }
            static::$redis = $redis;
        }
        return static::$redis;
    }

    public static function __callStatic($action, $arguments)
    {
        return call_user_func_array([static::getInstance(),$action],$arguments);
    }
}